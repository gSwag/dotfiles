### Dotfiles
My dot files and setup script.

### Features
- Sets up and configures ZSH + [oh-my-zsh](https://ohmyz.sh/)
- Installer script for language tools and terminal configuration
- Sets up [zellij (terminal multiplexer)](https://zellij.dev/)
- Sets up [helix editor](https://helix-editor.com/)

### Requirements
- Archlinux
- Install mono fonts - `ttf-jetbrains-mono-nerd` and `ttf-jetbrains-mono`

### Setup
Run `setup.sh` to go through the installation steps.

### Language Roadmap
[ ] Bash
[ ] C/C++
[ ] Golang
[ ] Python
[ ] JS/TS
[ ] CSS/SCSS
[ ] HTML
[ ] Markdown
[ ] JSON
[ ] YAML
[ ] TOML
