#!/usr/bin/bash

CUR_DIR=$(pwd)

back-to-pwd() {
	cd "${CUR_DIR[@]}" || exit 1
}

prompt() {
	while true; do
		read -p "$1 (y/n): " -r confirm

		case "$confirm" in
		[Yy]*)
			return 0
			;;
		[Nn]*)
			return 1
			;;
		*)
			echo "Please type Y or N"
			;;
		esac
	done
}

command-exists() {
	type "$1" &>/dev/null
}

# pre-requisites - nvm and node installed
init-node() {
	if ! command-exists "npm"; then
		source /usr/share/nvm/init-nvm.sh
	fi
}

# pre-requisites - go installed
init-golang() {
	export GOPATH=$HOME/Workspace/Go
}

update-packages() {
	if prompt "Update packages ?"; then
		sudo pacman -Syyu --noconfirm
	fi
}

install-prerequisites() {
	if prompt "Install pre-requisites ?"; then
		sudo pacman -S --needed --noconfirm \
			base-devel make git zsh curl less
	fi
}

install-yay() {
	if prompt "Install yay ?"; then
		git clone https://aur.archlinux.org/yay.git
		cd yay || exit 1
		makepkg -si

		back-to-pwd

		sudo rm -rf yay
	fi
}

update-packages-by-yay() {
	if prompt "Update packages by yay ?"; then
		yay -Syyu --noconfirm
	fi
}

setup-terminal() {
	if prompt "Setup terminal ?"; then
		chsh -s '/usr/bin/zsh'
		cd ~ || exit 1

		if [ -f .zshrc ]; then
			echo "ZSH config exists ! Backing up in zsh_backup directory."
			mkdir -pv zsh_backup
			cp .zshrc zsh_backup/
		fi

		if [ -d .oh-my-zsh ]; then
			echo "OH-MY-ZSH directory exists ! Backing up in zsh_backup directory."
			mkdir -pv zsh_backup
			cp -r .oh-my-zsh zsh_backup/
		fi

		rm -rf .zshrc .oh-my-zsh

		sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

		sed -i 's/^ZSH_THEME=.*$/ZSH_THEME="candy"/g' .zshrc

		back-to-pwd
	fi
}

install-dev-dependencies() {
	if prompt "Install developer dependencies ?"; then
		yay -S --noconfirm \
			gcc go nvm \
			zellij emacs-nativecomp

		init-node

		nvm install 20.14.0
	fi
}

setup-workspace() {
	if prompt "Setup workspace ?"; then
		cd || exit 1
		mkdir -pv Workspace/{Doc,Build,Git,Go,TMP}
		back-to-pwd
	fi
}

setup-envvars() {
	if prompt "Setup env vars ?"; then
		mkdir -pv ~/.config/environment.d

		# pam environments
		echo 'EDITOR=emacs -nw' >> ~/.config/environment.d/01-envvars.conf
		echo 'GOPATH=$HOME/Workspace/Go' >> ~/.config/environment.d/01-envvars.conf
		echo 'NVIM_DIR=$HOME/.nvm' >> ~/.config/environment.d/01-envvars.conf
		echo 'DOOM_DIR=$HOME/.emacs' >> ~/.config/environment.d/01-envvars.conf
		echo 'NODE_DIR=$NVIM_DIR/versions/node/v20.14.0' >> ~/.config/environment.d/01-envvars.conf
		echo 'PATH=$GOPATH/bin:$NODE_DIR/bin:$DOOM_DIR/bin:$PATH' >> ~/.config/environment.d/01-envvars.conf

		# zshrc
		echo 'source /usr/share/nvm/init-nvm.sh' >> ~/.zshrc
	fi
}

# TODO
install-language-servers() {
	# bash
	if prompt "Install Bash LSP ?"; then
		init-node
		npm i --location=global bash-language-server
	fi

	# golang
	if prompt "Install Golang LSP ?"; then
		init-golang
		go install golang.org/x/tools/gopls@latest
		go install github.com/nametake/golangci-lint-langserver@latest
		go install github.com/go-delve/delve/cmd/dlv@latest
		ln -sf $GOPATH/bin/golangci-lint-langserver $GOPATH/bin/golangci-lint
        go install github.com/fatih/gomodifytags@latest
        go install github.com/cweill/gotests/...@latest
		go install github.com/x-motemen/gore/cmd/gore@latest
        go install golang.org/x/tools/cmd/guru@latest
	fi

	# javascript/typescript
	if prompt "Install JS/TS/Html/CSS/JSON LSP ?"; then
		init-node
		npm i --location=global typescript typescript-language-server
		npm i --location=global @olrtg/emmet-language-server
		npm i --location=global vscode-langservers-extracted
	fi

	# yaml
	if prompt "Install YAML LSP ?"; then
		init-node
		npm i --location=global yaml-language-server
	fi

	# markdown
	if prompt "Install Markdown LSP ?"; then
		yay -S --noconfirm marksman
	fi

	# python
	if prompt "Install Python LSP ?"; then
		init-node
		npm i --location=global pyright
	fi
}

# TODO
install-formatters() {
	# bash
	if prompt "Install Bash formatters ?"; then
		init-golang
		go install mvdan.cc/sh/v3/cmd/shfmt@latest
	fi

	# golang
	if prompt "Install Golang formatters ?"; then
		init-golang
		go install golang.org/x/tools/cmd/goimports@latest
		go install mvdan.cc/gofumpt@latest
		go install github.com/segmentio/golines@latest
	fi

	# prettier
	if prompt "Install Prettier formatter ?"; then
		init-node
		npm i --location=global prettier \
			prettier-pnp \
			prettier-plugin-go-template \
			prettier-plugin-toml
	fi
}

# TODO
install-extras() {
	# golang
	if prompt "Install Golang extras ?"; then
		init-golang
		go install golang.org/x/tools/cmd/godoc@latest
	fi
}

# TODO
setup-editor-config() {
	if prompt "Setup editor (helix) ?"; then
		if [ -d "$HOME/.config/helix" ]; then
			echo "Editor configuration exists ! Backing up in $HOME/helix_backup"
			mkdir -pv "$HOME/helix_backup"
			cp -r "$HOME/.config/helix" "$HOMR/helix_backup"
			rm -rf "$HOME/.config/helix"
		fi
		ln -sfv "$CUR_DIR/.config/helix" "$HOME/.config/"

		if command-exists "simple-completion-language-server"; then
			simple-completion-language-server fetch-external-snippets
			simple-completion-language-server validate-snippets
		fi
	fi

	# TODO
	# if prompt "Setup editor (emacs) ?"; then
	# fi
}

main() {
	update-packages
	install-prerequisites
	install-yay
	update-packages-by-yay
	setup-terminal
	install-dev-dependencies
	setup-workspace
	setup-envvars
	install-language-servers
	install-formatters
	install-extras
	setup-editor-config
}

main
